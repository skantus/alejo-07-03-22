import OrderItem from './OrderItem';
import { render } from '@testing-library/react-native';
import React from 'react';
import { OrderType } from 'src/components/types';

describe('OrderItem', () => {
  it('should render correctly', () => {
    const { queryByTestId } = render(
      <OrderItem
        index={0}
        item={{
          price: '42,028.00',
          size: '8,381',
          total: '144,045',
          percent: '2%',
        }}
        type={OrderType.Ask}
      />,
    );

    const component = queryByTestId('orderItem');

    expect(component?.props.children).toBeTruthy();
    expect(component?.props.testID).toEqual('orderItem');
  });
});
