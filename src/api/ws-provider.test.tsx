import { WSProvider } from './ws-provider';
import { render, waitFor } from '@testing-library/react-native';
import { useEffect } from 'react';
import React, { FC } from 'react';
import { Text, View } from 'react-native';
import { BITCOIN_API } from 'src/constants';
import { WebSocketMock } from 'src/test-utils';

describe('WSProvider', () => {
  const send = jest.fn();
  const Socket = WebSocketMock(send);
  const server = new Socket();
  let ChildComponent: FC;

  beforeEach(() => {
    ChildComponent = () => {
      const startConnection = () => {
        server.send = (message: string) => {
          send(message);
          setTimeout(() => {
            server.onmessage({ data: JSON.stringify(BITCOIN_API) });
          });
        };
        return server;
      };

      useEffect(() => {
        startConnection();
      }, []);

      return (
        <View>
          <Text testID="test">Test</Text>
        </View>
      );
    };
  });

  it('should request the data from bitcoin API message', async () => {
    render(
      <WSProvider>
        <ChildComponent />
      </WSProvider>,
    );

    await waitFor(async () => {
      await server.connected;
      server.send(JSON.stringify(JSON.stringify(BITCOIN_API)));
    });

    expect(send).toBeCalledWith(
      expect.stringContaining(
        '"{\\"event\\":\\"subscribe\\",\\"feed\\":\\"book_ui_1\\",\\"product_ids\\":[\\"PI_XBTUSD\\"]}"',
      ),
    );
  });
});
