import { mocked_data } from 'src/api/mocks/data';

export const WebSocketMock = (send: Function = jest.fn()) => {
  return class WebSocket {
    readyState = 1;
    CONNECTING = 0;
    OPEN = 1;
    CLOSING = 2;
    CLOSED = 3;
    send = (_: string) => {};
    onmessage = jest.fn();
    onopen = jest.fn();
    connected = jest.fn();
    constructor() {
      setTimeout(() => {
        this.onopen();
        this.onmessage({ data: JSON.stringify(mocked_data) });
      });
      this.send = (message: string) => {
        send(message);
        this.onmessage({ data: JSON.stringify(mocked_data) });
      };
    }
  };
};
