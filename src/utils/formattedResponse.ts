import { FeedType, OrderType } from 'src/api';
import { decimalPart } from 'src/utils/decimalPart';
import { numberFormat } from 'src/utils/numberFormat';

export const formattedResponse = (list: OrderType[]): FeedType[] => {
  let totalAccumulated = 0;
  return list
    ?.map(order => {
      const value = order[0];
      const size = order[1];
      const total = (totalAccumulated += size);

      const price = numberFormat(2).format(value);
      const decimals = decimalPart[price.split(',')[0].length];
      const chartPercent = ((total * decimals) / value).toFixed(0);

      return {
        price,
        size: numberFormat().format(size),
        total: numberFormat().format(total),
        percent: `${chartPercent}%`,
      };
    })
    .filter(item => item.size !== '0');
};
