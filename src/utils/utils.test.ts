import { getUpdatedList } from './utils';
import { mocked_data } from 'src/api/mocks/data';

describe('formattedResponse', () => {
  const expectedResult = {
    asks: [
      { percent: '1%', price: '34,074.00', size: '1,728', total: '1,728' },
      { percent: '8%', price: '34,080.00', size: '23,999', total: '25,727' },
      { percent: '12%', price: '34,080.50', size: '14,999', total: '40,726' },
      { percent: '13%', price: '34,082.50', size: '3,299', total: '44,025' },
      { percent: '15%', price: '34,086.00', size: '7,000', total: '51,025' },
      { percent: '16%', price: '34,086.50', size: '4,393', total: '55,418' },
      { percent: '34%', price: '34,087.00', size: '58,951', total: '114,369' },
      { percent: '34%', price: '34,087.50', size: '1,200', total: '115,569' },
      { percent: '34%', price: '34,089.50', size: '1,013', total: '116,582' },
      { percent: '35%', price: '34,090.50', size: '2,500', total: '119,082' },
      { percent: '36%', price: '34,091.00', size: '2,482', total: '121,564' },
      { percent: '36%', price: '34,092.00', size: '990', total: '122,554' },
    ],
    bids: [
      { percent: '1%', price: '34,061.00', size: '1,958', total: '1,958' },
      { percent: '4%', price: '34,060.00', size: '12,944', total: '14,902' },
      { percent: '5%', price: '34,056.50', size: '3,216', total: '18,118' },
      { percent: '8%', price: '34,055.00', size: '9,131', total: '27,249' },
      { percent: '9%', price: '34,052.50', size: '4,146', total: '31,395' },
      { percent: '10%', price: '34,052.00', size: '2,500', total: '33,895' },
      { percent: '11%', price: '34,051.50', size: '2,053', total: '35,948' },
      { percent: '11%', price: '34,051.00', size: '1,994', total: '37,942' },
      { percent: '13%', price: '34,050.50', size: '5,304', total: '43,246' },
      { percent: '35%', price: '34,050.00', size: '77,026', total: '120,272' },
      { percent: '86%', price: '34,049.50', size: '173,388', total: '293,660' },
      { percent: '87%', price: '34,049.00', size: '3,000', total: '296,660' },
    ],
    productId: 'PI_XBTUSD',
    spread: 'Spread: 13.0  (0.04%)',
  };

  it('should format the list properly', () => {
    expect(
      getUpdatedList({ snapshotData: undefined, data: mocked_data }),
    ).toEqual(expectedResult);
  });
});
