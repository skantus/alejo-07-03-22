import { formattedResponse } from './formattedResponse';
import { FeedType, OrderType } from 'src/api';

describe('formattedResponse', () => {
  const list: OrderType[] = [
    [37737, 4400],
    [35736, 1204],
  ];

  const expectedResult: FeedType[] = [
    {
      price: '37,737.00',
      size: '4,400',
      total: '4,400',
      percent: '1%',
    },
    {
      price: '35,736.00',
      size: '1,204',
      total: '5,604',
      percent: '2%',
    },
  ];

  it('should format the list properly', () => {
    expect(formattedResponse(list)).toEqual(expectedResult);
  });
});
