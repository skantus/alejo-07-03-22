export const decimalPart: { [key: number]: number } = {
  1: 1,
  2: 10,
  3: 100,
  4: 1000,
  5: 10000,
  6: 100000,
  7: 1000000,
};
