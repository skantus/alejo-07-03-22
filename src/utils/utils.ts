import { calculatePercent } from './calculatePercent';
import { formattedResponse } from './formattedResponse';
import { getFilteredList } from './getFilteredList';
import { numberFormat } from './numberFormat';
import { FeedsResponse, WebsocketResponse } from 'src/api';

const getUpdatedList = ({
  snapshotData,
  data,
}: {
  snapshotData: WebsocketResponse;
  data: WebsocketResponse;
}): FeedsResponse => {
  const { bids, asks } = getFilteredList(snapshotData, data);

  const highestBids = bids[0][0];
  const lowestAsks = asks[0][0];

  const spread = numberFormat(1).format(lowestAsks - highestBids);
  const spreadPercent = calculatePercent(lowestAsks, highestBids);

  return {
    productId: data?.product_id,
    spread: `Spread: ${spread}  (${spreadPercent}%)`,
    bids: formattedResponse(bids),
    asks: formattedResponse(asks),
  };
};

export { getUpdatedList };
