# Orderbook

### Feature Set

- ✅  Totals correctly calculated.
- ✅  Spread correctly calculated.
- ✅  Sorting of the orderbook.
- ✅ Depth graph
- ✅  Zero size levels are correctly removed from the orderbook.
- ✅  Switching between contracts works as expected.

### Design

- ✅  Design mirrors.
- ✅  Responsive orderbook orientation.
- ✅  Rerender requests are throttled.
- ✅  Rerender throttling time is variable based on device performance.

### Optimisations

- ✅  UI maintains steady 55-60 FPS with no spikes.
- ✅  Rendering performance

## How to run

Run on iOS:

- `yarn`
- `cd ios && pod install` (if you’re using Xcode with Rosseta) - or - `sudo arch -x86_64 gem install ffi` 
- `yarn start`
- `yarn ios` - or - open from Xcode.

<p align="center">
    <img src="https://bitbucket.org/skantus/alejo-07-03-22/raw/444e515cb47bb2057f1f0df1acacbbca349a0b71/sources/screenshot.png" width=300 />
</p>

